<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Tambah Perusahaan</title>
    <!--google font-->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@100;300;400;700;900&display=swap" rel="stylesheet">
    <!--fontawesome-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!--custom css-->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}" />
    <style>
        .card-add-company {
            width: auto !important;
            height: auto !important;
        }
    </style>
</head>
<body>

<div class="page_container">
    <div class="card card-center card-add-company">
        <div class="card-header">
            <h1>Tambah Perusahaan</h1>
            <a href="{{ route('index') }}" class="btn btn-white">Kembali</a>
        </div>
        <div class="card-body p-12">
            <form action="{{ route('insert') }}" method="post" class="form__wrapper" style="width: 400px">
                @csrf
                <div class="form__input">
                    <label for="input-nama">Nama Perusahaan</label>
                    <input id="input-nama" type="text" name="nama" />
                </div>
                <div class="form__input">
                    <label for="input-alamat">Alamat</label>
                    <textarea id="input-alamat" name="alamat"></textarea>
                </div>
                <button type="submit" class="btn btn-green btn-block">Tambah</button>
        </div>
        </form>
    </div>
    @if(boolval($result))
        <script>
            const result = "{{ $result['msg'] }}";
            alert(result);
        </script>
    @endif
</div>
</div>

</body>
</html>