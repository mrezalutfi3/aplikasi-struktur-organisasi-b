<table>
    <thead>
        <tr>
            <th>ID</th>
            <th>Nama Perusahaan</th>
            <th>Alamat Perusahaan</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($companies as $company)
            <tr>
                <td>{{ $company->id }}</td>
                <td>{{ $company->nama }}</td>
                <td>{{ $company->alamat }}</td>
            </tr>
        @endforeach
    </tbody>
</table>