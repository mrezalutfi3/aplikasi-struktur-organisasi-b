<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CompanyController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('company')->group(function () {
    // view route
    Route::get('/', [CompanyController::class, 'index'])->name('index');
    Route::get('/create', [CompanyController::class, 'create'])->name('create');
    Route::get('/edit/{id}', [CompanyController::class, 'edit'])->name('edit');
    Route::get('/export_excel', [CompanyController::class, 'export_excel'])->name('export_excel');
    Route::get('/export_pdf', [CompanyController::class, 'export_pdf'])->name('export_pdf');

    // action route
    Route::post('/insert', [CompanyController::class, 'insert'])->name('insert');
    Route::post('/update/{id}', [CompanyController::class, 'update'])->name('update');
    Route::post('/delete/{id}', [CompanyController::class, 'delete'])->name('delete');
});