<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Daftar Karyawan</title>
    <!--google font-->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@100;300;400;700;900&display=swap" rel="stylesheet">
    <!--fontawesome-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!--custom css-->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}" />
</head>
<body>

    <div class="page_container">
        <div class="card card-center">
            <div class="card-header">
                <h1>Manajemen Karyawan</h1>
                <div>
                    <a href="{{ route('create') }}" class="btn btn-white"><i class="fas fa-plus"></i>&nbsp;&nbsp;&nbsp;Tambah Karyawan</a>
                </div>
            </div>
            <div class="card-body">
                <div class="table-header__wrapper">
                    <table>
                        <thead>
                            <tr>
                                <th>Nama</th>
                                <th>Atasan</th>
                                <th>Perusahaan</th>
                                <th class="action">Aksi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div class="table-body__wrapper">
                    <table>
                        <tbody>
                        @foreach($employees as $employee)
                            <tr>
                                <td>{{ $employee->nama }}</td>
                                <td>{{ $employee->atasan }}</td>
                                <td>{{ $employee->perusahaan }}</td>
                                <td class="action">
                                    <a href="{{ route('edit', ['id' => $employee->id]) }}" class="btn btn-icon btn-warning"><i class="fas fa-edit"></i></a>
                                    <button class="delete-btn btn btn-danger btn-icon" data-id="{{ $employee->id }}"><i class="fas fa-trash"></i></button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script>
        // PHP VARIABLES START

        const baseUrl = "{{ url('') }}";

        // PHP VARIABLES END

        const deleteBtns = document.querySelectorAll('.delete-btn');

        deleteBtns.forEach((elm) => {
            elm.addEventListener('click', () => {
                const id = elm.getAttribute('data-id');

                if( confirm('Anda yakin ingin menghapus karyawan ini?') ) {

                    axios
                        .post(baseUrl+'/employee/delete/'+id)
                        .then((res) => {
                            alert(res.data.msg);
                            location.reload();
                        })
                        .catch((error) => {
                            console.error(error);
                        });

                }

            })
        });

    </script>
</body>
</html>