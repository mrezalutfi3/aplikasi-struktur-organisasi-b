<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Edit Karyawan</title>
    <!--google font-->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@100;300;400;700;900&display=swap" rel="stylesheet">
    <!--fontawesome-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!--custom css-->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}" />
    <style>
        .card-edit-employee {
            width: auto !important;
            height: auto !important;
        }
    </style>
</head>
<body>

    <div class="page_container">
        <div class="card card-center card-edit-employee">
            <div class="card-header">
                <h1>Edit Karyawan</h1>
                <a href="{{ route('index') }}" class="btn btn-white">Kembali</a>
            </div>
            <div class="card-body p-12">
                <form action="{{ route('update', ['id' => $employee->id]) }}" method="post" class="form__wrapper" style="width: 400px">
                    @csrf
                    <div class="form__input">
                        <label for="input-nama">Nama</label>
                        <input id="input-nama" type="text" name="nama" value="{{ $employee->nama }}"/>
                    </div>
                    <div class="form__input">
                        <label for="select-atasan">Atasan</label>
                        <select name="atasan_id" id="select-atasan">
                            <option value="0">Tidak Ada</option>
                            @foreach($supervisors as $supervisor)
                                @if($supervisor->id == $employee->atasan_id)
                                    <option value="{{ $supervisor->id }}" selected>{{ $supervisor->nama }}</option>
                                @else
                                    <option value="{{ $supervisor->id }}">{{ $supervisor->nama }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form__input">
                        <label for="select-atasan">Perusahaan</label>
                        <select name="company_id" id="select-perusahaan">
                            @foreach($companies as $company)
                                @if($company->id == $employee->company_id)
                                    <option value="{{ $company->id }}" selected>{{ $company->nama }}</option>
                                @else
                                    <option value="{{ $company->id }}">{{ $company->nama }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <button type="submit" class="btn btn-green btn-block">Update</button>
                </form>
            </div>
        </div>
    </div>

    @if(boolval($result))
        <script>
            const result = "{{ $result['msg'] }}";
            alert(result);
        </script>
    @endif
</body>
</html>