<?php

namespace App\Http\Controllers;

use App\Exports\CompanyExport;
use Illuminate\Http\Request;
use App\Models\Company;
use Maatwebsite\Excel\Facades\Excel;

class CompanyController extends Controller
{
    public function index(){
        $companies = Company::all();

        return view('company.index',compact('companies'));
    }
       
    public function create(Request $request){
        $result = $request->session()->get('result');

        return view('company.create', compact('result'));
    }
   
    public function insert(Request $request){
        $status = null;
        $msg = null;

        try {
            $company = new Company();
            $company->nama = $request->get('nama');
            $company->alamat = $request->get('alamat');

            if( !$company->save() )
            {
                throw new \Exception();
            }

            $status = 'success';
            $msg = 'Sukses menambahkan perusahaan';


        } catch(\Exception $e){
            $status = 'failed';
            $msg = 'Gagal menambahkan perusahaan';
        }

        $request->session()->flash('result', compact('status', 'msg') );
        return redirect()->back();
    }
   
    public function delete($id){
        $status = null;
        $msg = null;

        try {
            $company = Company::find($id);
            if( !$company->delete() )
            {
                throw new \Exception();
            }
            $status = 'success';
            $msg = 'Berhasil menghapus data perusahaan';

        } catch (\Exception $e) {
            $status = 'success';
            $msg = 'Gagal menghapus data perusahaan';
        }

        return response(compact('status', 'msg'));
    }
   
    public function edit(Request $request, $id){
        $company = Company::find($id);

        $result = $request->session()->get('result');

        return view('company.edit',compact('company', 'result'));
    }
   
    public function update(Request $request, $id){
        $status = null;
        $msg = null;

        try {
            $company = Company::find($id);
            $company->nama = $request->get('nama');
            $company->alamat = $request->get('alamat');

            if( !$company->save() )
            {
                throw new \Exception();
            }

            $status = 'success';
            $msg = 'Sukses menupdate perusahaan';


        } catch(\Exception $e){
            $status = 'failed';
            $msg = 'Gagal menupdate perusahaan';
        }

        $request->session()->flash('result', compact('status', 'msg') );
        return redirect()->back();
    }

    public function export_excel() {
        return Excel::download(new CompanyExport, 'Company.xlsx');
    }

    public function export_pdf() {
        return Excel::download(new CompanyExport, 'Company.pdf', \Maatwebsite\Excel\Excel::DOMPDF);
    }
}
