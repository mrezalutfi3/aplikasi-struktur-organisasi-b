<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Employee;
use App\Models\Company;
use App\Exports\EmployeeExport;
use Maatwebsite\Excel\Facades\Excel;

class EmployeeController extends Controller
{
    public function index(){
        $employees = DB::table('employee as ea')
                        ->leftJoin('employee as eb', 'ea.atasan_id', '=', 'eb.id')
                        ->leftJoin('company as c', 'ea.company_id', '=', 'c.id')
                        ->select('ea.id', 'ea.nama as nama', 'eb.nama as atasan', 'c.nama as perusahaan')
                        ->get();
        //    dd($employees);
        return view('employee.index', compact('employees'));
    }

    public function create(Request $request){
        $supervisors = Employee::select('id', 'nama')->get();
        $companies = Company::select('id', 'nama')->get();

        $result = $request->session()->get('result');

        return view(
            'employee.create',
            compact('supervisors', 'companies', 'result')
        );
    }
   
    public function insert(Request $request){
        $status = null;
        $msg = null;

        try {
            $employee = new Employee();
            $employee->nama = $request->get('nama');
            $employee->company_id = intval($request->get('company_id'));
            if( $request->get('atasan_id') != '0' ) {
                $employee->atasan_id = intval($request->get('atasan_id'));
            }

            if( !$employee->save() )
            {
                throw new \Exception();
            }

            $status = 'success';
            $msg = 'Sukses menambahkan karyawan';


        } catch(\Exception $e){
            $status = 'failed';
            $msg = 'Gagal menambahkan karyawan';
        }

        $request->session()->flash('result', compact('status', 'msg') );
        return redirect()->back();
    }
   
    public function delete($id){
        $status = null;
        $msg = null;

        try {
            $employee = Employee::find($id);
            if( !$employee->delete() )
            {
                throw new \Exception();
            }
            $status = 'success';
            $msg = 'Berhasil menghapus data karyawan';

        } catch (\Exception $e) {
            $status = 'success';
            $msg = 'Gagal menghapus data karyawan';
        }

        return response(compact('status', 'msg'));
    }
   
    public function edit(Request $request, $id){
        $employee = Employee::find($id);

        $supervisors = Employee::select('id', 'nama')
                                ->where('id', '<>', $employee->id)
                                ->get();

        $companies = Company::select('id', 'nama')->get();

        $result = $request->session()->get('result');

        return view('employee.edit',compact('employee', 'supervisors', 'companies', 'result'));
    }
   
    public function update(Request $request, $id){
        $status = null;
        $msg = null;

        try {
            $employee = Employee::find($id);
            $employee->nama = $request->get('nama');
            $employee->company_id = intval($request->get('company_id'));
            if( $request->get('atasan_id') != '0' ) {
                $employee->atasan_id = intval($request->get('atasan_id'));
            }

            if( !$employee->save() )
            {
                throw new \Exception();
            }

            $status = 'success';
            $msg = 'Sukses menupdate karyawan';


        } catch(\Exception $e){
            $status = 'failed';
            $msg = 'Gagal menupdate karyawan';
        }

        $request->session()->flash('result', compact('status', 'msg') );
        return redirect()->back();
    }

    public function export_excel() {
        return Excel::download(new EmployeeExport, 'Employee.xlsx');
    }

    public function export_pdf() {
        return Excel::download(new EmployeeExport, 'Employee.pdf', \Maatwebsite\Excel\Excel::DOMPDF);
    }
}
