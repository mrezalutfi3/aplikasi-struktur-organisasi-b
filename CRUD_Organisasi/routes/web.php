<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\EmployeeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('company')->group(function () {
    // view route
    Route::get('/', [CompanyController::class, 'index'])->name('company.index');
    Route::get('/create', [CompanyController::class, 'create'])->name('company.create');
    Route::get('/edit/{id}', [CompanyController::class, 'edit'])->name('company.edit');
    Route::get('/export_excel', [CompanyController::class, 'export_excel'])->name('company.export_excel');
    Route::get('/export_pdf', [CompanyController::class, 'export_pdf'])->name('company.export_pdf');

    // action route
    Route::post('/insert', [CompanyController::class, 'insert'])->name('company.insert');
    Route::post('/update/{id}', [CompanyController::class, 'update'])->name('company.update');
    Route::post('/delete/{id}', [CompanyController::class, 'delete'])->name('company.delete');
});

Route::prefix('employee')->group(function () {
    // view route
    Route::get('/', [EmployeeController::class, 'index'])->name('employee.index');
    Route::get('/create', [EmployeeController::class, 'create'])->name('employee.create');
    Route::get('/edit/{id}', [EmployeeController::class, 'edit'])->name('employee.edit');
    Route::get('/export_excel', [EmployeeController::class, 'export_excel'])->name('employee.export_excel');
    Route::get('/export_pdf', [EmployeeController::class, 'export_pdf'])->name('employee.export_pdf');

    // action route
    Route::post('/insert', [EmployeeController::class, 'insert'])->name('employee.insert');
    Route::post('/update/{id}', [EmployeeController::class, 'update'])->name('employee.update');
    Route::post('/delete/{id}', [EmployeeController::class, 'delete'])->name('employee.delete');
});