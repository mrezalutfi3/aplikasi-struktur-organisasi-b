<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Manajemen Perusahaan</title>
    <!--google font-->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@100;300;400;700;900&display=swap" rel="stylesheet">
    <!--fontawesome-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!--custom css-->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}" />
</head>
<body>

    <div class="page_container">
        <div class="card card-center">
            <div class="card-header">
                <h1>Manajemen Perusahaan</h1>
                <div>
                    <a href="{{ route('company.create') }}" class="btn btn-white"><i class="fas fa-plus"></i>&nbsp;&nbsp;&nbsp;Tambah Perusahaan</a>
                </div>
            </div>
            <div class="card-body">
                <div>
                    <a href="{{ route('company.export_excel') }}" class="btn btn-green">Download as Excel</a>
                    <a href="{{ route('company.export_pdf') }}" class="btn btn-danger">Download as PDF</a>
                </div>
                <div class="table-header__wrapper">
                    <table>
                        <thead>
                        <tr>
                            <th>Nama Perusahaan</th>
                            <th>Alamat</th>
                            <th class="action">Aksi</th>
                        </tr>
                        </thead>
                    </table>
                </div>
                <div class="table-body__wrapper">
                    <table>
                        <tbody>
                        @foreach($companies as $company)
                            <tr>
                                <td>{{ $company->nama }}</td>
                                <td>{{ $company->alamat }}</td>
                                <td class="action">
                                    <a href="{{ route('company.edit', ['id' => $company->id]) }}" class="btn btn-icon btn-warning"><i class="fas fa-edit"></i></a>
                                    <button data-id="{{ $company->id }}" class="delete-btn btn btn-icon btn-danger"><i class="fas fa-trash"></i></button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script>
        // PHP VARIABLES START

        const baseUrl = "{{ url('') }}";

        // PHP VARIABLES END

        const deleteBtns = document.querySelectorAll('.delete-btn');

        deleteBtns.forEach((elm) => {
            elm.addEventListener('click', () => {
                const id = elm.getAttribute('data-id');

                if( confirm('Anda yakin ingin menghapus perusahaan ini?') ) {

                    axios
                        .post(baseUrl+'/company/delete/'+id)
                        .then((res) => {
                            alert(res.data.msg);
                            location.reload();
                        })
                        .catch((error) => {
                            console.error(error);
                        });

                }

            })
        });

    </script>

</body>
</html>