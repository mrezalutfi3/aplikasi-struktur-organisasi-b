<table>
    <thead>
        <tr>
            <th>ID</th>
            <th>Nama</th>
            <th>Atasan</th>
            <th>Perusahaan</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($employees as $employee)
            <tr>
                <td>{{ $employee->id }}</td>
                <td>{{ $employee->nama }}</td>
                <td>{{ $employee->atasan != null ? $employee->atasan->nama : 'Tidak Ada' }}</td>
                <td>{{ $employee->perusahaan->nama }}</td>
            </tr>
        @endforeach
    </tbody>
</table>